const express = require('express');
const app=express();

app.engine('html', require('ejs').renderFile); //if not 'cannot find module html'
app.set('view engine', 'html')

app.get('/', function(req, res) {
  res.render('index');
});

