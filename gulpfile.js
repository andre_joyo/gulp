const gulp = require('gulp');

const ts = require('gulp-typescript');

const sass = require('gulp-sass');

const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');

const browserSync = require('browser-sync').create();

gulp.task('hi', (done)=> {
    console.log("Hello");
    done();
})
//----------------------------------------------------
gulp.task('typescript', ()=> {
    const tsResult = gulp.src('./*.ts')
    .pipe(ts({
    	noImplicitAny: true
 	}));
    return tsResult.js.pipe(gulp.dest('built/local'));
});

/*gulp.task('watch', function() {
    gulp.watch('./*.ts', ['typescript']);
});*/

//gulp.task('default', ['typescript', 'watch']);
//----------------------------------------------------
gulp.task('sass', function(){
	console.log('sass executed');
 	return gulp.src('app/scss/example.scss')
 		.pipe(sass()) // Converts Sass to CSS with gulp-sass
 		.pipe(gulp.dest('app/css'))//destination
});

//For all sass files
//gulp.task('sass', function() {
// return gulp.src('app/scss/**/*.scss') // Gets all files ending with .scss in app/scss and children dirs
// .pipe(sass())
// .pipe(gulp.dest('app/css'))
//})
//----------------------------------------------------
gulp.task('minify-css', () => {
  return gulp.src('app/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename((file) => {//console.log(file.basename)
    	file.basename+='.min'
    }))
    .pipe(gulp.dest('app/dist'));
});
//----------------------------------------------------
gulp.task('serve', function() {
    browserSync.init({
        server: "./"
    });
    gulp.watch("./app/scss/*.scss").on('change', gulp.parallel('sass'));
    gulp.watch("./app/scss/*.scss").on('change', browserSync.reload);
    gulp.watch("./*.html").on('change', browserSync.reload);
});
gulp.task('default', gulp.parallel('hi','serve'));
//----------------------------------------------------
/*gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});*/
